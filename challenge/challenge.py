from flask import Flask, request, flash, make_response
import csv
from io import StringIO
import sys
sys.path.append('./challenge')
from Model import Model
from pandas import read_csv
import io

model = None

app = Flask(__name__)
app.secret_key = "437"


# Hello world test endpoint
@app.route('/hello_world')
def hello_world():
    return "Hello world!"


# Training endpoint
@app.route('/genres/train', methods=['POST'])
def train():
    file = io.BytesIO(request.get_data())
    df = read_csv(file)
    global model
    model = Model()
    model.train(df)
    return "Model trained \n"


# Prediction endpoint
@app.route('/genres/predict', methods=['POST'])
def predict():
    file = io.BytesIO(request.get_data())
    df = read_csv(file)
    global model
    if not model:
        return "A model needs to be trained \n"
    else:
        prediction = model.predict(df)
        csv_file = prediction.to_csv(index=False)
        output = make_response(csv_file)
        return output

