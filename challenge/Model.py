from collections import Counter
import tensorflow as tf
from keras import models, layers, metrics
from keras.utils import to_categorical
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from sklearn.preprocessing import MultiLabelBinarizer
import numpy as np
from pandas import DataFrame
import string
import nltk
nltk.download('stopwords')
import sys
sys.path.append('./challenge')


# Main model class to be used to train a TensorFlow model
class Model:
    def __init__(self):

        # Lock the randomness in the model
        tf.random.set_seed(1)

        # Define the model architecture

        # Sequential model
        self.model = models.Sequential()

        self.model.add(layers.Embedding(input_dim=self._get_words_synopsis_amount(),
                                        output_dim=100,
                                        input_length=self._get_longest_synopsis_len()))

        self.model.add(layers.Conv1D(filters=64, kernel_size=8, activation='relu'))
        self.model.add(layers.MaxPooling1D(pool_size=2))
        self.model.add(layers.Conv1D(filters=64, kernel_size=8, activation='relu'))
        self.model.add(layers.MaxPooling1D(pool_size=2))

        self.model.add(layers.Flatten())

        self.model.add(layers.Dense(units=256))
        self.model.add(layers.Activation(activation='relu'))
        self.model.add(layers.Dropout(0.5))
        self.model.add(layers.Dense(units=256))
        self.model.add(layers.Activation(activation='relu'))
        self.model.add(layers.Dropout(0.5))
        # Output layer
        self.model.add(layers.Dense(units=len(self._get_genres()), activation='sigmoid'))
        #self.model.add(layers.Softmax()) # NO SOFTMAX!

        # Compilation
        self.model.compile(optimizer=tf.keras.optimizers.Adam(learning_rate=0.001),
                           loss='binary_crossentropy',
                           metrics=[self.top_5_categorical_accuracy, 'categorical_accuracy'])

        # Initialize Tokenizer
        self.tokenizer = Tokenizer(num_words=self._get_words_synopsis_amount())

        # Training session parameters
        self.train_percentage = 0.8
        self.train_epochs = 20 #20
        self.train_batch_size = 64

    # Train the model.
    def train(self, data_frame):
        # Shuffle the rows in the data_frame
        data_frame = data_frame.sample(frac=1, random_state=1)
        # Preprocess the synopses and labels
        X = self.preprocess_data(data_frame.synopsis)
        Y = self.preprocess_targets(data_frame.genres)

        # Calculate the amount of entries and make train and valid partition
        n_entries = len(data_frame.index)
        valid_index = int(self.train_percentage*n_entries)
        x_train, x_valid = (X[:valid_index], X[valid_index + 1:])
        y_train, y_valid = (Y[:valid_index], Y[valid_index + 1:])

        # Transform and batch the datasets
        train_dataset = tf.data.Dataset.from_tensor_slices((x_train, y_train))
        valid_dataset = tf.data.Dataset.from_tensor_slices((x_valid, y_valid))
        train_dataset = train_dataset.batch(self.train_batch_size)
        valid_dataset = valid_dataset.batch(len(train_dataset))

        # Train the model
        self.model.fit(train_dataset,
                       batch_size=self.train_batch_size,
                       epochs=self.train_epochs,
                       validation_data=valid_dataset,
                       callbacks=[tf.keras.callbacks.EarlyStopping('val_loss', patience=3)])

    # Predict with the trained model.
    def predict(self, data_frame):
        # Define new resulting data frame and copy the movie ID's
        result = DataFrame(columns=['movie_id', 'predicted_genres'])
        result['movie_id'] = data_frame['movie_id']
        # Preprocess the synopses
        X = self.preprocess_data(data_frame.synopsis)
        # Make predictions
        prediction = self.model.predict(X)
        prediction = self._to_top_genres(prediction)
        prediction = list(map(lambda x: " ".join(x), prediction))
        for i in range(len(prediction)):
            result['predicted_genres'][i] = prediction[i]
        return result


    # Preprocess the data using word embeddings
    def preprocess_data(self, data):
        # Transform sentence strings to list of words
        data = data.map(lambda x: x.split(" "))
        # Remove non-alphabetic characters
        data = data.apply(lambda x: list(map(lambda y: y if y.isalpha() else "", x)))
        # Remove punctuations
        data = data.apply(lambda x: list(map(lambda y: y.translate(str.maketrans('', '', string.punctuation)), x)))
        # Remove known stop words in English
        stop_words = set(nltk.corpus.stopwords.words('english'))
        data = data.apply(lambda x: list(map(lambda y: y if not y in stop_words else "", x)))
        # Remove short words
        data = data.apply(lambda x: list(map(lambda y: y if len(y) > 1 else "", x)))
        min_occur = 2
        count_tokenizer = Tokenizer()
        count_tokenizer.fit_on_texts(data)
        #occurrences = dict(sorted(count_tokenizer.word_docs.items(), key=lambda item: item[1], reverse=True))
        occurrences = count_tokenizer.word_docs
        data = data.apply(lambda x: list(map(lambda y: y if occurrences[y.lower()] >= min_occur else "", x)))
        # Remove all empty strings
        data = data.apply(lambda x: self._remove_empty(x))
        # Fit data to tokenizer
        self.tokenizer.fit_on_texts(data)
        # Tokenize the data
        data = self.tokenizer.texts_to_sequences(data)
        # Pad the data
        data = pad_sequences(data, padding='post', maxlen=self._get_longest_synopsis_len())
        return data

    # Preprocess the targets using one hot encoding
    def preprocess_targets(self, targets):
        # Split the strings up
        targets = list(map(lambda x: x.split(" "), targets))
        # Encode the targets using MLB
        mlb = MultiLabelBinarizer()
        targets = mlb.fit_transform(targets)
        return targets









    # Get all genres present in the training data. If the variable hard_coded is true, then a precalculated list is used
    # with all genres in it. If set to false, a list of genres is calculated.
    def _get_genres(self, data_frame=None, hard_coded=True):
        if hard_coded:
            return ['Action', 'Adventure', 'Animation', 'Children', 'Comedy', 'Crime', 'Documentary', 'Drama',
                    'Fantasy', 'Film-Noir', 'Horror', 'IMAX', 'Musical', 'Mystery', 'Romance', 'Sci-Fi', 'Thriller',
                    'War', 'Western']
        else:
            genres = data_frame.genres.unique()
            genres = list(map(lambda x: x.split(" "), genres))
            genres = [item for sublist in genres for item in sublist]
            genres = list(set(genres))
            return genres.sort()

    # Calculate the length in words of the longest synopsis in the dataset.
    def _get_longest_synopsis_len(self, data_frame=None, hard_coded=True):
        if hard_coded:
            return 115
        else:
            synopses = list(data_frame.synopsis)
            lengths = list(map(lambda x: len(x.split(" ")), synopses))
            return np.array(lengths).max()

    # Calculate the amount of unique words in a dataset.
    def _get_words_synopsis_amount(self, data_frame=None, hard_coded=True):
        if hard_coded:
            return 27489
        else:
            synopses = list(data_frame.synopsis)
            words = list(map(lambda x: x.split(" "), synopses))
            words = list(set([item for sublist in words for item in sublist]))
            return words

    def _remove_empty(self, l):
        result = []
        for item in l:
            if not item == "":
                result.append(item)
        return result

    def top_5_categorical_accuracy(self, y_true, y_pred):
        return metrics.top_k_categorical_accuracy(y_true, y_pred, k=5)

    def _to_top_genres(self, prediction):
        genres = self._get_genres()
        prediction = np.array(prediction)
        result = []
        for pred in prediction:
            tops = pred.argsort()[-5:][::-1]
            result.append(list(map(lambda x: genres[x], tops)))
        return result


