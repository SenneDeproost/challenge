import pandas as pd
import numpy as np
import string
from keras.preprocessing.text import Tokenizer
from keras_preprocessing.text import text_to_word_sequence
from keras.layers.experimental.preprocessing import TextVectorization
from keras.preprocessing.sequence import pad_sequences

from Model import Model

data = pd.read_csv('../train.csv')
model = Model()
model.train(data)
exit()